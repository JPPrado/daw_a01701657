<?php
    
    function _e($string){
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
        //echo htmlentities($string,ENT_QUOTES,'UTF-8');
    }

    session_start();
    if (isset($_SESSION["name"])) {
        header("location:sessioned.php");
        
    } else {
        include("_header.html");
        include("_footer.html");
    }
?>