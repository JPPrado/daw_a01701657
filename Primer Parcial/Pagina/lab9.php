<?php

    function prom($arry){
        $prom = array_sum($arry)/count($arry);
        return $prom;
    }

    function med($arry){
        $med;
        rsort($arry);
        if(count($arry)%2 == 0){
            $med = ($arry[count($arry)/2])+($arry[(count($arry)/2)-1]);
            $med = $med/2;
        } else{
            $med = $arry[(count($arry)/2-.5)];
        }
        return $med;
    }
    
    function lista($arry){
        $size = count($arry);
        sort($arry);
        echo "<h3>Lista Menor a Mayor</h3>";
        echo "<ul>";
        for($i=0; $i<$size; $i++) {
            echo "<li>".$arry[$i]."</li>";
        }
        echo "</ul>";
        
        rsort($arry);
        echo "<h3>Lista Mayor a Menor</h3>";
        echo "<ul>";
        for($i=0; $i<$size; $i++) {
            echo "<li>".$arry[$i]."</li>";
        }
        echo "</ul>";
        echo "Promedio: ".prom($arry)."<br>Media: ".med($arry);
        
    }

    function tab(){
        echo "<table><tr><th> número </th><th> cuadrado </th><th> cubo </th></tr>";
        for($i=1; $i<20; $i++){
            echo "<tr><td> ". $i ." </td><td> ". ($i * $i) ." </td><td> ". ($i * $i * $i) ." </td></tr>";
        }
        echo "</table>";
    }

    function inv($a){
        $inverse = 0;
        while($a != 0){
            $res = $a%10;
            $a = floor($a/10);
            $inverse = ($inverse * 10) + $res;
        }
        return $inverse;
    }

    
    include("_header.html");
    include("lab9.html");
    
?>