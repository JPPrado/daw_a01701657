<?php
    function connectDB(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "Materiales";
        
        $con = mysqli_connect($servername,$username,$password, $dbname);
        
        if(!$con){
            die("connection failed: ".mysqli_connect_error());
        }
        
        return $con;
    }

    function closeDb($mysql){
        mysqli_close($mysql);
    }

    function getMateriales(){
        $conn = connectDB();
        
        $sql = "SELECT clave, descripcion, precio FROM materiales";
        
        $result = mysqli_query($conn, $sql);
        
        closeDb($conn);
        
        return $result;
    }

    function getMaterialsByDescripcion($mats){
        $conn = connectDB();
        
        $sql = "SELECT clave, descripcion, precio FROM materiales WHERE descripcion LIKE '%".mats."%'";
        
        $result = mysqli_query($conn, $sql);
        
        closeDb($conn);
        
        return $result;
    }

    function getCheapestMats($cheap_price){
        $conn = connectDB();
        
        $sql = "SELECT clave, descripcion, precio FROM materiales WHERE precio <= '".$cheap_price."'";
        
        $result = mysqli_query($conn, $sql);
        
        closeDb($conn);
        
        return $result;
    }

    function getMatsTabla(){
        $db = connectDB();
        $query = 'SELECT * FROM materiales';
        $mats = $db->query($query);
        $tabla = '<table>
            <thead>
            <tr>
            <th>Clave</th>
            <th>Descripcion</th>
            <th>Precio</th>
            </tr>
            </thead>
            <tbody>';
        while($fila=mysqli_fetch_array($mats, MYSQLI_BOTH)){
            $tabla.='
            <tr>
            <td>'.$fila["Clave"].'</td>
            <td>'.$fila["Descripcion"].'</td>
            <td>'.$fila["Precio"].'</td>
            </tr>';
        }
            mysqli_free_result($mats);
            closeDb($db);
            $tabla.="</tbody></table>";
        return $tabla;
    }

    function getMatsTablaPrecio(){
        $db = connectDB();
        $query = 'SELECT * FROM materiales WHERE Precio <= 370';
        $mats = $db->query($query);
        $tabla = '<table>
            <thead>
            <tr>
            <th>Clave</th>
            <th>Descripcion</th>
            <th>Precio</th>
            </tr>
            </thead>
            <tbody>';
        while($fila=mysqli_fetch_array($mats, MYSQLI_BOTH)){
            $tabla.='
            <tr>
            <td>'.$fila["Clave"].'</td>
            <td>'.$fila["Descripcion"].'</td>
            <td>'.$fila["Precio"].'</td>
            </tr>';
        }
            mysqli_free_result($mats);
            closeDb($db);
            $tabla.="</tbody></table>";
        return $tabla;
    }

?>