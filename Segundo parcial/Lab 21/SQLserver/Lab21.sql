IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaMaterial' AND type = 'P')
                DROP PROCEDURE creaMaterial
            
            
            CREATE PROCEDURE creaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
            GO


EXECUTE creaMaterial 5000,'Martillos Acme',250,15

SELECT *
FROM Materiales

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'editarMaterial' AND type = 'P')
                DROP PROCEDURE editarMaterial
            GO
            
            CREATE PROCEDURE editarMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                UPDATE Materiales 
				SET Descripcion=@udescripcion, Costo=@ucosto, PorcentajeImpuesto=@uimpuesto
				WHERE Clave=@uclave
            GO

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminarMaterial' AND type = 'P')
                DROP PROCEDURE eliminarMaterial
            GO
            
            CREATE PROCEDURE eliminarMaterial
                @uclave NUMERIC(5,0)
            AS
                DELETE  
				FROM materiales 
				WHERE Clave=@uclave
            GO

IF EXISTS (SELECT name FROM sysobjects 
                                       WHERE name = 'queryMaterial' AND type = 'P')
                                DROP PROCEDURE queryMaterial
                            GO
                            
                            CREATE PROCEDURE queryMaterial
                                @udescripcion VARCHAR(50),
                                @ucosto NUMERIC(8,2)
                            
                            AS
                                SELECT * FROM Materiales WHERE descripcion 
                                LIKE '%'+@udescripcion+'%' AND costo > @ucosto 
                            GO

EXECUTE queryMaterial 'Lad',20