-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2017 a las 21:01:55
-- Versión del servidor: 5.5.57-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `c9`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Estado`
--

CREATE TABLE IF NOT EXISTS `Estado` (
  `Id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `Estado` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `Estado`
--

INSERT INTO `Estado` (`Id_estado`, `Estado`) VALUES
(1, 'infeccion'),
(2, 'coma'),
(3, 'transformacion'),
(4, 'muerto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `EstadoZombi`
--

CREATE TABLE IF NOT EXISTS `EstadoZombi` (
  `Id_estado` int(11) NOT NULL,
  `Id_zombi` int(11) NOT NULL,
  `Ehora` time NOT NULL,
  `Efecha` date NOT NULL,
  PRIMARY KEY (`Ehora`,`Efecha`),
  UNIQUE KEY `Id_zombi` (`Id_zombi`),
  UNIQUE KEY `Id_estado` (`Id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zombis`
--

CREATE TABLE IF NOT EXISTS `zombis` (
  `Id_zombi` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(15) NOT NULL,
  `Zfecha` date NOT NULL,
  `Zhora` time NOT NULL,
  PRIMARY KEY (`Id_zombi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `zombis`
--

INSERT INTO `zombis` (`Id_zombi`, `Nombre`, `Zfecha`, `Zhora`) VALUES
(1, 'Alex', '2017-10-27', '20:15:13'),
(2, 'Robin', '2017-10-27', '20:15:13'),
(3, 'Zombie', '2017-10-27', '20:15:38'),
(5, 'democles', '2017-10-27', '20:16:44'),
(6, 'psycokiller', '2017-10-27', '20:16:44'),
(7, 'seriouslee', '2017-10-27', '20:16:44'),
(8, 'dio', '2017-10-27', '20:16:44'),
(9, 'Beto', '2017-10-27', '21:01:15'),
(10, 'Pedro', '2017-10-27', '21:01:15');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `EstadoZombi`
--
ALTER TABLE `EstadoZombi`
  ADD CONSTRAINT `EstadoZombi_ibfk_2` FOREIGN KEY (`Id_zombi`) REFERENCES `Estado` (`Id_estado`),
  ADD CONSTRAINT `EstadoZombi_ibfk_1` FOREIGN KEY (`Id_estado`) REFERENCES `zombis` (`Id_zombi`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
