Revisa el contenido de la tabla materiales y determina si existe alguna inconsistencia en el contenido de la tabla. �Cu�l es? �A qu� se debe? 
Hay redundancia de datos. Por que no hay una llave definida.

�Qu� ocurri�? 
Se le agreg� una llave

�Qu� informaci�n muestra esta consulta?

Muestra la llave de materiales

�Qu� sentencias utilizaste para definir las llaves primarias?
ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)

�Qu� sentencias utilizaste para definir este constrait?
ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave, RFC, Numero, Fecha)

�Qu� particularidad observas en los valores para clave, rfc y numero? 
No tienen el largo establecido.

�C�mo responde el sistema a la inserci�n de este registro?
De manera normal

�Qu� significa el mensaje que emite el sistema? 
Que esta llave no existe para la tabla materiales

�Qu� significado tiene la sentencia anterior? 
liga las tablas por medio de una llave for�nea

�Qu� uso se le est� dando a GETDATE()?
Obtener la fecha del sistema

�Tiene sentido el valor del campo de cantidad?
No, no lo tiene

�C�mo responde el sistema?
No agrega el registro

�Qu� significa el mensaje? 
Que la 'cantidad' del registro es err�nea

INTEGRIDAD REFERENCIAL
La integridad referencial se refiere a que la clave externa de una tabla debe ser igual a alguna columna de la tabla referenciada.
Garantizando que no haya redundancia de datos.

https://msdn.microsoft.com/es-MX/library/aa292166(v=vs.71).aspx



