BULK INSERT a1701657.a1701657.[Materiales]
   FROM 'e:\wwwroot\a1701657	\materiales.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

SELECT *FROM Materiales

BULK INSERT a1701657.a1701657.[Proyectos]
   FROM 'e:\wwwroot\a1701657\proyectos.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )