SET DATEFORMAT DMY
SELECT SUM((M.Costo+M.PorcentajeImpuesto)*Cantidad)as Total
FROM Entregan, (SELECT Costo, Clave, PorcentajeImpuesto FROM Materiales)M
WHERE Entregan.Clave = M.Clave AND Entregan.Fecha BETWEEN '01/01/1997' AND '31/12/1997' 

SELECT P.RazonSocial, COUNT(e.RFC) as NumeroEntregas, SUM(E.Cantidad*(M.costo+M.porcentajeImpuesto)) as Importe
FROM Proveedores P, Entregan E, Materiales M
WHERE P.RFC = E.RFC AND M.Clave = E.Clave
GROUP BY P.RazonSocial

SELECT M.Clave, M.Descripcion, COUNT(E.clave) as Cantidad, MIN(E.Cantidad) as MinCantidad, MAX(E.cantidad) as MaxCant
FROM Materiales M, Entregan E
WHERE M.Clave=E.Clave
GROUP BY M.Clave, M.Descripcion
/*No se puede sacar el promedio de las entregas mayores a 400 ya que generar�a un error cuando est�s no lleguen a este n�mero*/

SELECT P.RazonSocial, M.Clave, M.Descripcion, AVG(E.Cantidad) as CantidadPromedio
FROM Materiales M, Entregan E, Proveedores P
WHERE P.RFC=E.RFC AND M.Clave=E.Clave
GROUP BY P.RazonSocial, M.Clave, M.Descripcion
HAVING AVG(E.Cantidad) > 500

SELECT P.RazonSocial, M.Clave, M.Descripcion, AVG(E.Cantidad) as CantidadPromedio
FROM Materiales M, Entregan E, Proveedores P
WHERE P.RFC=E.RFC AND M.Clave=E.Clave
GROUP BY P.RazonSocial, M.Clave, M.Descripcion
HAVING AVG(E.Cantidad) > 370 or AVG(E.Cantidad) < 450

INSERT INTO Materiales VALUES(1,'Aros Met�licos', 500, 8);
INSERT INTO Materiales VALUES(2,'Abrazadera', 15, 2);
INSERT INTO Materiales VALUES(3,'Tornillo 1/2', 7,1);
INSERT INTO Materiales VALUES(4,'Tuerca 1/6', 3, 1);
INSERT INTO Materiales VALUES(5,'Mecate', 12, 2);

SELECT M.Clave, M.Descripcion
FROM Materiales M, Entregan E
WHERE M.Clave NOT IN
(SELECT M.Clave
 From Materiales M, Entregan E
 WHERE M.Clave=E.Clave
 GROUP BY M.Clave)
 GROUP BY M.Clave, M.Descripcion

 SELECT P.RazonSocial
 FROM Proveedores P, Proyectos Pr, Entregan E
 WHERE P.RFC=E.RFC AND Pr.Numero=E.Numero AND Pr.Denominacion='Vamos mexico'
 AND P.RazonSocial IN(SELECT P.RazonSocial
					  FROM Proveedores p, Proyectos Pr, Entregan E
					  WHERE P.RFC=E.RFC AND Pr.Numero=E.Numero AND Pr.Denominacion='Queretaro Limpio')
GROUP BY P.RazonSocial

SELECT M.Clave, M.Descripcion
FROM Materiales M
WHERE M.Clave NOT IN(SELECT M.Clave
					 FROM Materiales M, Entregan E, Proyectos P
					 WHERE M.Clave=E.Clave AND P.Numero=E.Numero AND P.Denominacion='CIT Yucatan')
GROUP BY M.Clave, M.Descripcion

SELECT P.RazonSocial, AVG(E.Cantidad) as Promedio
FROM Proveedores P, Entregan E
WHERE P.RFC=E.RFC
GROUP BY P.RazonSocial
HAVING AVG(E.Cantidad) > (SELECT AVG(E.Cantidad)
						  FROM Proveedores P, Entregan E
						  WHERE P.RFC=E.RFC AND P.RFC='AAAA800101   ')
/*El proveedor de RFC 'VAGO780901'*/

SELECT P.RazonSocial, Prom.Promedio
FROM Proveedores P, (SELECT E.RFC, AVG(E.Cantidad) as Promedio
					 FROM Entregan E
					 GROUP BY E.RFC) Prom
WHERE P.RFC = Prom.RFC AND Prom.Promedio>(SELECT Prom.Promedio
										  FROM (SELECT E.RFC, AVG(E.Cantidad) as Promedio
												FROM Entregan E
												GROUP BY E.RFC) Prom
										  WHERE RFC = 'AAAA800101')

select razonSocial from Proveedores