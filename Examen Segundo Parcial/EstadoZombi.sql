-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2017 a las 21:00:23
-- Versión del servidor: 5.5.57-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `c9`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `EstadoZombi`
--

CREATE TABLE IF NOT EXISTS `EstadoZombi` (
  `Id_estado` int(11) NOT NULL,
  `Id_zombi` int(11) NOT NULL,
  `Ehora` time NOT NULL,
  `Efecha` date NOT NULL,
  PRIMARY KEY (`Ehora`,`Efecha`),
  UNIQUE KEY `Id_zombi` (`Id_zombi`),
  UNIQUE KEY `Id_estado` (`Id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `EstadoZombi`:
--   `Id_zombi`
--       `Estado` -> `Id_estado`
--   `Id_estado`
--       `zombis` -> `Id_zombi`
--

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `EstadoZombi`
--
ALTER TABLE `EstadoZombi`
  ADD CONSTRAINT `EstadoZombi_ibfk_2` FOREIGN KEY (`Id_zombi`) REFERENCES `Estado` (`Id_estado`),
  ADD CONSTRAINT `EstadoZombi_ibfk_1` FOREIGN KEY (`Id_estado`) REFERENCES `zombis` (`Id_zombi`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
