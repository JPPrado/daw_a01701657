function rToabs(){
    document.getElementById("abs").style.position = "absolute";
}

function toItalic(){
    setTimeout(function(){document.getElementById("abs").style.fontStyle = "italic";},3000);  
}

function textChange(){
    document.querySelector("#texto").style.fontStyle = "oblique"; 
}

function aiuda(){
    window.alert("Estas fueron las referencias encontradas por cuenta del creador.");
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}