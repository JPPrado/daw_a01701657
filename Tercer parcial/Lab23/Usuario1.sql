CREATE TABLE Clientes_Banca(
NoCuenta varchar(5) NOT NULL,
Nombre varchar(30),
Saldo numeric(10,2)
);

CREATE TABLE Tipos_Movimiento(
ClaveM varchar(2) NOT NULL,
Descripcion varchar(30),
);

CREATE TABLE Movimientos(
NoCuenta varchar(5) NOT NULL,
ClaveM varchar(2) NOT NULL,
Fecha datetime,
Monto numeric(10,2)
);

ALTER TABLE Clientes_Banca ADD CONSTRAINT Cuenta PRIMARY KEY (NoCuenta); 
ALTER TABLE Tipos_Movimiento ADD CONSTRAINT TipoMov PRIMARY KEY (ClaveM);
ALTER TABLE Movimientos ADD CONSTRAINT CuentaMov FOREIGN KEY (NoCuenta) REFERENCES Clientes_Banca(NoCuenta);
ALTER TABLE Movimientos ADD CONSTRAINT MovTipo FOREIGN KEY (ClaveM) REFERENCES Tipos_Movimiento(ClaveM);

BEGIN TRANSACTION PRUEBA1
INSERT INTO CLIENTES_BANCA VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO CLIENTES_BANCA VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO CLIENTES_BANCA VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION PRUEBA1 

SELECT * FROM CLIENTES_BANCA 

SELECT * FROM CLIENTES_BANCA 

SELECT * FROM CLIENTES_BANCA where NoCuenta='001' 


BEGIN TRANSACTION PRUEBA3
INSERT INTO TIPOS_MOVIMIENTO VALUES('A','Retiro Cajero Automatico');
INSERT INTO TIPOS_MOVIMIENTO VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION PRUEBA3

BEGIN TRANSACTION PRUEBA4
INSERT INTO MOVIMIENTOS VALUES('001','A',GETDATE(),500);
UPDATE CLIENTES_BANCA SET SALDO = SALDO -500
WHERE NoCuenta='001'
COMMIT TRANSACTION PRUEBA4

SELECT * FROM Clientes_Banca
SELECT * FROM Movimientos

BEGIN TRANSACTION PRUEBA5
INSERT INTO CLIENTES_BANCA VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Camino Ortiz',5000);
INSERT INTO CLIENTES_BANCA VALUES('007','Oscar Perez Alvarado',8000);

IF @@ERROR = 0
COMMIT TRANSACTION PRUEBA5
ELSE
BEGIN
PRINT 'A transaction needs to be rolled back'
ROLLBACK TRANSACTION PRUEBA5
END

SELECT * FROM Clientes_Banca

GO
CREATE PROCEDURE  REGISTRAR_RETIRO_CAJERO 
	@uNoCuenta varchar(5),
	@uMonto	numeric(10,2)
AS
	BEGIN TRANSACTION REGISTRO
		INSERT INTO Movimientos VALUES(@uNoCuenta,'A',GETDATE(),@uMonto);
		UPDATE Clientes_Banca SET Saldo = Saldo-@uMonto WHERE NoCuenta=@uNoCuenta;
	COMMIT TRANSACTION REGISTRO
GO

GO
CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA
	@uNoCuenta varchar(5),
	@uMonto numeric(10,2)
AS
	BEGIN TRANSACTION REGISTRO2
		INSERT INTO Movimientos VALUES(@uNoCuenta,'B',GETDATE(),@uMonto);
		UPDATE Clientes_Banca SET Saldo = Saldo+@uMonto WHERE NoCuenta=@uNoCuenta;
	COMMIT TRANSACTION REGISTRO2
GO

