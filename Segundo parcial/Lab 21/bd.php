<?php

function _e($string) {
    return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    //echo htmlentities($string, ENT_QUOTES, 'UTF-8');
}
    function connectDB(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "Materiales";
        
        $con = mysqli_connect($servername,$username,$password, $dbname);
        $con->set_charset("utf8");
        if(!$con){
            die("connection failed: ".mysqli_connect_error());
        }
        
        return $con;
    }

    function closeDb($mysql){
        mysqli_close($mysql);
    }
    function getMatDesc($id){
        $db = connectDB();
        $query = 'SELECT Descripcion FROM materiales WHERE Clave='.$id.' GROUP BY Clave';
        $mats = $db->query($query);
        $row = $fila["Descripcion"];
        mysqli_free_result($mats);
        closeDb($db);
        return $row;
    }
    
    function getMatsTabla(){
        $db = connectDB();
        $query = 'SELECT * FROM materiales GROUP BY Clave';
        $mats = $db->query($query);
        $tabla = '<table class="table table-condensed">
            <thead>
            <tr>
            <th>Clave</th>
            <th>Descripcion</th>
            <th>Precio</th>
            </tr>
            </thead>
            <tbody>';
        while($fila=mysqli_fetch_array($mats, MYSQLI_BOTH)){
            $tabla.='
            <tr>
            <td>'.$fila["Clave"].'</td>
            <td id="d'.$fila["Clave"].'">'.$fila["Descripcion"].'</td>
            <td id="p'.$fila["Clave"].'">'.$fila["Precio"].'</td>
            <td><a href="delete.php/?id='.$fila["Clave"].'"><button type="button" class="btn btn-danger glyphicon glyphicon-trash" data-toggle="modal" data-target="#getMats id='.$fila["Clave"].'"></button></a></td>
            <td><button type="button" class="btn btn-warning pull-right glyphicon glyphicon-pencil" href="#modal-3" data-toggle="modal" data-dismiss="modal" id='.$fila["Clave"].' onClick="getID(this.id)"></button></td>
            </tr>';
        }
            mysqli_free_result($mats);
            closeDb($db);
            $tabla.="</tbody></table>";
        return $tabla;
    }
    function guardarRegistro($Descripcion, $Precio){
    $db = connectDB();
    
    // insert command specification 
    $query='INSERT INTO materiales (`Descripcion`, `Precio`) VALUES (?,?) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ss", $Descripcion, $Precio)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 

    
    closeDB($db);
    }
    
    function editarRegistro($descripcion, $Precio, $clave){
    $db = connectDB();
    
    // insert command specification 
    $query='UPDATE Materiales SET descripcion=?, precio=? WHERE clave=?';
    // Preparing the statement 
    if (!($statement = mysqli_query($db,"CALL update_mats('$descripcion','$Precio','$clave')"))) {/**STORED PROCEDURE*/
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sss", $descripcion, $precio, $clave)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($db) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
 
    function getRegistro($db, $registroId){
    //Specification of the SQL query
    $query='SELECT * FROM Materiales WHERE id="'.$registroId.'" GROUP BY '.$registroID.'';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    return $fila;
    }
    
    closeDb($db);
    }
    
    function deleteRegistro($Clave){
        $db = connectDB();
        // insert command specification 
        $query='DELETE FROM materiales WHERE Clave=?';
        // Preparing the statement 
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("s", $Clave)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // update execution
        if ($statement->execute()) {
            echo 'There were ' . mysqli_affected_rows($db) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }

        closeDB($db);
    }
    
    function getDropdownMats($tabla, $order_by_field, $selected=-1) {
     $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM '.$tabla.' ORDER BY '.$order_by_field;
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    $select = '<div class="input-field col-sm-12">
                <select name="'.$tabla.'_id">';
      
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        if ($selected == $fila["id"]) {
            $select .= '<option value="'.$fila["id"].'" selected>'.$fila["$order_by_field"].'</option>';
        } else {
            $select .= '<option value="'.$fila["id"].'">'.$fila["$order_by_field"].'</option>';
        }
        
    }
    $select .= '</select><label for="'.$tabla.'">'.$tabla.'</label></div>';
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    return $select;
    }
?>