<?php
    
    function _e($string) {
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
        //echo htmlentities($string, ENT_QUOTES, 'UTF-8');
    }
    
    if(isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["mensaje"])){
        if(empty($_POST["name"]) || empty($_POST["email"]) || empty($_POST["mensaje"])){
            $msg = "Llena todos los campos";
            include("_header.html");
            include("_empty.html");
            include("_footer.html");
        }else{
            $n = _e($_POST["name"]);
            $em = _e($_POST["email"]);
            $m = _e($_POST["mensaje"]);
            include("_header.html");
            include("_main.html");
            include("_footer.html");
        }
    }else{
        header("Location:lab11.php");
    }
?>