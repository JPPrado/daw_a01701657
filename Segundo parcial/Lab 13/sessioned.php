
<?php
    session_start();

function _e($string) {

 return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');

  //echo htmlentities($string, ENT_QUOTES, 'UTF-8');
}
    
    if (isset($_SESSION["user"])) {
        include("_header.html");
        include("_main.html");
        include("_footer.html");

        
    }  else if (isset($_POST['name'])) {
            $fn=_e($_POST["name"]);
        
        $target_dir = "media/";
        $target_file = $target_dir . basename($_FILES["foto"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["send"])) {
            $check = getimagesize($_FILES["foto"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["foto"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["foto"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        
        $_SESSION["foto"] = $target_file;
          $_SESSION["acepto"] = true;
        
       
        
        include("_header.html");
        include("_main.html");
        include("_footer.html");
    
    } else {
        
        header("location:index.php");
    }
    
    
    
    
?>

